import * as React from 'react';
import ItemContainer from './containers/item-container';

class App extends React.Component {
  render() {
    return (
      <div>
        <ItemContainer id="one" text="some text" />
      </div>
    );
  }
}

export default App;
