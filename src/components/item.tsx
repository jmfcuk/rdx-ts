import * as React from 'react';

interface ItemProps {
    id: string;
    text: string;
}

const Item = (props: ItemProps) => {

    return <div>Id: {props.id}, text: {props.text}</div>;
};

export default Item;
