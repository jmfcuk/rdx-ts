import * as React from 'react';
import Item from '../components/item';

interface ItemContainerProps {
    id: string;
    text: string;
}

interface ItemContainerState {
    id: string;
    text: string;
}

class ItemContainer extends React.Component<ItemContainerProps, ItemContainerState> {

    state: ItemContainerState;

    constructor(props: ItemContainerProps) {
        super(props);
    }

    render(): JSX.Element {

        return <Item id={this.props.id} text={this.props.text} />;
    }
} 

export default ItemContainer;
